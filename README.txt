INSTALLATION
------------

First you need to register your product here 
http://code.google.com/apis/console.
Then create on selest API projects section.

Click ‘API Access’ from the let menu. 
Click on ‘Create an OAuth 2.0 client ID…’ button on the page that just opened up.

Fill in a name for the project. When a user clicks to retrieve the contacts,
this product name will be shown on the conformation page 

Now you will be taken to Client ID settings page.
Choose ‘Web application’ radio button for the Application type.

In ‘Your site or hostname’ block, choose ‘http://’ or ‘https://’
as per your requirement. Redirect URL must be http://Your domain/gmail-contact-list/oauth.
Domain name will be your javascript origin.

Now you have registered your product and
you can view the API credentials for your web application.
Note down the Client ID, Client secret. 
Please fill setting form here admin/config/services/gmail-contact-list
with your gmail details.

You can check import links user/%user/importer or gmail-contact-list/importer.
