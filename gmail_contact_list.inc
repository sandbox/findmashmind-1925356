<?php
/**
 * @file
 * All custom function goes here.
 */

/**
 * Define settings variables.
 */
define('GMAIL_CLIENT_KEY', variable_get('gmail_client_key', NULL));
define('GMAIL_CLIENT_SECRET', variable_get('gmail_contact_secret', NULL));
define('GMAIL_CONTACT_MAX', variable_get('gmail_contact_max', 25));

/**
 * Message sending function. Call to Drupal_mail() function.
 */
function gmail_contact_list_mail_send($form_values) {
  global $user;
  // All system mails need to specify the module and template key (mirrored from
  // hook_mail()) that the message they want to send comes from.
  $module = 'gmail_contact_list';
  $key = 'gmail_message';

  // Specify 'to' and 'from' addresses.
  $to_array = '';
  foreach ($form_values['recipient'] as $recipient) {
    if ($recipient != "0") {
      $to_array .= $recipient . ',';
    }
  }

  $to = $to_array;
  $from = $user->mail;

  $params = $form_values;

  $language = language_default();
  $send = TRUE;
  // Send the mail, and check for success. Note that this does not guarantee
  // message delivery; only that there were no PHP-related issues encountered
  // while sending.
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);

  if ($result['result'] == TRUE) {
    drupal_set_message(t('Your message has been sent.'));
    unset($_SESSION['list_of_emails']);
    drupal_goto('user/' . $user->uid . '/importer');
  }
  else {
    drupal_set_message(t('There was a problem sending your message and it was not sent'), 'error');
  }
}

/**
 * Implements hook_mail().
 */
function gmail_contact_list_mail($key, &$message, $params) {
  global $user;

  // Each message is associated with a language, which may or may not be the
  // current user's selected language, depending on the type of e-mail being
  // sent. This $options array is used later in the t() calls for subject
  // and body to ensure the proper translation takes effect.
  $options = array(
    'langcode' => $message['language']->language,
  );

  switch ($key) {
    // Send a simple message from the contact form.
    case 'gmail_message':
      $message['subject'] = t('E-mail sent from @site-name', array('@site-name' => variable_get('site_name', 'Drupal')), $options);
      // Note that the message body is an array, not a string.
      $message['body'][] = t('@name sent you the following message:', array('@name' => $user->name), $options);
      $message['body'][] = check_plain($params['message']);
      break;
  }
}

/**
 * Handle redirect call back from google.
 */
function _gmail_contact_list_auth() {
  global $base_url;
  $client_id = GMAIL_CLIENT_KEY;
  $client_secret = GMAIL_CLIENT_SECRET;
  $redirect_uri = $base_url . '/gmail-contact-list/oauth';
  $max_results = GMAIL_CONTACT_MAX;

  $auth_code = $_GET["code"];

  $fields = array(
    'code' => urlencode($auth_code),
    'client_id' => urlencode($client_id),
    'client_secret' => urlencode($client_secret),
    'redirect_uri' => urlencode($redirect_uri),
    'grant_type' => urlencode('authorization_code'),
  );

  $post = '';
  foreach ($fields as $key => $value) {
    $post .= $key . '=' . $value . '&';
  }
  $post = rtrim($post, '&');

  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
  curl_setopt($curl, CURLOPT_POST, 5);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

  $result = curl_exec($curl);
  curl_close($curl);

  $response = json_decode($result);
  $accesstoken = $response->access_token;

  $url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results=' . $max_results . '&oauth_token=' . $accesstoken;

  $xmlresponse = gmail_contact_list_get_contents($url);

  if ((strlen(stristr($xmlresponse, 'Authorization required')) > 0) && (strlen(stristr($xmlresponse, 'Error')) > 0)) {
    drupal_set_message(t('Something went wrong. Please try reloading the page.'), 'error');
    exit();
  }

  $cont = "";
  $xml = new SimpleXMLElement($xmlresponse);
  $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
  $result = $xml->xpath('//gd:email');

  $cont = array();
  foreach ($result as $title) {
    $cont[] = (string) $title->attributes()->address;
  }
  drupal_save_session(TRUE);
  $_SESSION['list_of_emails'] = $cont;
  drupal_goto('user/' . $user->uid . '/importer/gsend');
}

/**
 * @params - URL to fetch
 * return the transfer as a string of the return value of curl_exec().
 */
function gmail_contact_list_get_contents($url) {
  $curl = curl_init();
  $user_agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';

  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($curl, CURLOPT_USERAGENT, $user_agent);
  curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
  curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);
  curl_setopt($curl, CURLOPT_TIMEOUT, 10);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

  $contents = curl_exec($curl);

  curl_close($curl);
  return $contents;
}

/**
 * Link for make request from Google.
 * 
 * Use client ID and redirect url.
 */
function gmail_contact_list_importer() {
  global $base_url;
  $redirect_url = $base_url . '/gmail-contact-list/oauth';

  return l(t('Click to Import Gmail Contacts'), 'https://accounts.google.com/o/oauth2/auth?client_id=' . GMAIL_CLIENT_KEY . '&redirect_uri=' . $redirect_url . '&scope=https://www.google.com/m8/feeds/&response_type=code',
  array('attributes' => array('class' => array('link-gmail-contact-list'))));
}

/**
 * Message send form. Fields - Message and email list.
 */
function gmail_contact_list_form() {
  $gmail_contact_list_array = array();
  global $base_url;

  if (isset($_SESSION['list_of_emails'])) {
    $gmail_contact_list_array = $_SESSION['list_of_emails'];
  }

  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('You can use this form to send message to your gmail friends'),
  );

  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Add your message'),
    '#required' => TRUE,
    '#default_value' => "Please check this site " . $base_url,
    '#description' => "Please enter your message here.",
  );

  $form['recipient'] = array(
    '#type' => 'checkboxes',
    '#options' => drupal_map_assoc($gmail_contact_list_array),
    '#title' => t('Select email contact to send message'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send message',
  );

  return $form;
}

/**
 * Message send form submit function.Pass form values to mail send function.
 */
function gmail_contact_list_form_submit($form, &$form_state) {
  gmail_contact_list_mail_send($form_state['values']);
}
